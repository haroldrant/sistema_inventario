<%@page import="Dto.Historialinventario"%>
<%@page import="Dto.Producto"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.time.ZoneId"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Locale"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.List"%>
<%@page import="java.time.LocalDate"%>

<html lang="es">
    <head>
        <title>Administraci�n Pizza Bohemia</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <!-- VENDOR CSS -->
        <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="../assets/vendor/linearicons/style.css">
        <link rel="stylesheet" href="../assets/vendor/chartist/css/chartist-custom.css">
        <!-- MAIN CSS -->
        <link rel="stylesheet" href="../assets/css/main.css">
        <link rel="stylesheet" href="../assets/css/styles.css">
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
        <!-- ICONS -->
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" href="../assets/img/logo.png"/>
        <script src="../js/index.js" type="text/javascript"></script>
        
        <jsp:include page="MostrarPro.do"></jsp:include>
    </head>

    <body>
        <%
            HttpSession sesion = request.getSession();
            if (sesion.getAttribute("user") == null) {
                response.sendRedirect("../index.jsp");
            }
            sesion.setMaxInactiveInterval(60 * 10);
        %>
        <!-- WRAPPER -->
        <div id="wrapper">
            <!-- NAVBAR -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="brand">
                    <a href="../index.jsp"><img src="../assets/img/logo1.png" alt="Bohemia Logo" class="img-responsive logo"></a>
                </div>
                <div class="container-fluid">
                    <div class="navbar-btn">
                        <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
                    </div>
                    <div class="navbar-btn navbar-btn-right">
                        <a href="../Cerrar.do"><i class="lnr lnr-exit"></i> <span>Cerrar Sesi&oacute;n</span></a>
                    </div>
                </div>
            </nav>
            <!-- END NAVBAR -->
            <!-- LEFT SIDEBAR -->
            <div id="sidebar-nav" class="sidebar">
                <div class="sidebar-scroll">
                    <nav>
                        <ul class="nav">
                            <li><a href="../index.jsp"><i class="lnr lnr-home"></i> <span>Inicio</span></a></li>
                            <!-- Men� de productos -->
                            <li>
                                <a href="MostrarIngrediente.do"><i class="fa fa-database"></i> <span>Registro Inventario</span></a>                                
                            </li>
                            <!-- Men� de novedades -->
                            <li><a href="jsp/registroVentas.jsp" class="active"><i class="lnr lnr-dice"></i> <span>Realizar venta</span></a>

                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- END LEFT SIDEBAR -->
            <!-- MAIN -->
            <div class="main">                
                <!-- MAIN CONTENT -->
                <div class="main-content">
                    <div class="container-fluid" id="contenedor">
                        <div class="panel">


                            <div class="panel-body">
                                <%
                                    Historialinventario h = (Historialinventario) request.getSession().getAttribute("historial");
                                    Historialinventario fin = (Historialinventario) request.getSession().getAttribute("historialFin");
                                    if (fin != null) {

                                %>
                                <h4 class="panel-subtitle">Ya has cerrado inventario por hoy, toma un descanso!</h4>                                
                                <a href="../index.jsp" role="button" class="btn btn-primary"><i class="fa fa-arrow-up" aria-hidden="true"></i> <span>Volver</span></a>

                                <%} else if (h != null) {%>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="selecton brdr-b-primary">
                                            <li><a class="active" href="#" data-select="pizza"><b>PIZZA</b></a></li>
                                        </ul>
                                    </div><!--col-sm-12-->
                                </div>
                                <table class="table pizza">
                                    <thead>
                                        <tr class="table-info">
                                            <th class="table-info" scope="col">Producto</th>
                                            <th class="table-info" scope="col">Tama�o</th>
                                            <th class="table-info" scope="col">Precio</th>
                                            <th class="table-info" scope="col">Venta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            List<Producto> pro = (List) request.getSession().getAttribute("producto");
                                            DecimalFormat formatea = new DecimalFormat("###,###.##");
                                            List<Boolean> pre = (List) request.getSession().getAttribute("preparado");
                                            for (int i = 0; i < pro.size(); i++) {
                                        %>
                                        <tr class="table-info">
                                            <td class="table-info"><%=pro.get(i).getNombre()%></td>
                                            <td class="table-info"><%=pro.get(i).getTam()%></td>
                                            <td class="table-info a-pre">$ <%=formatea.format(pro.get(i).getPrecio())%> COP</td>
                                            <td class="table-info">
                                                <%
                                                    if (pre.get(i)) {
                                                %>
                                                <a href="compra.do?id=<%=pro.get(i).getId()%>" role="button" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Realizar venta</a>
                                                <%
                                                } else {
                                                %>
                                                <button href="#" class="btn btn-success" disabled>Ingredientes insuficientes</button>
                                                <%
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                        <%
                                            }

                                            request.getSession().removeAttribute("preparado");
                                            request.getSession().removeAttribute("producto");
                                        %>
                                    </tbody>
                                </table>
                                <%
                                } else {%>
                                <h4 class="panel-subtitle">Vaya, parece que no has agregado inventario hoy</h4>
                                <a href="MostrarIngrediente.do" role="button" class="btn btn-primary"><i class="fa fa-arrow-up" aria-hidden="true"></i> <span>Agregar Inventario</span></a>
                                <a href="../index.jsp" role="button" class="btn btn-primary"><i class="fa fa-arrow-up" aria-hidden="true"></i> <span>Volver</span></a>
                                <%
                                    }
                                    request.getSession().removeAttribute("historial");
                                    request.getSession().removeAttribute("historialFin");
                                %>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MAIN CONTENT -->
            </div>
            <!-- END MAIN -->
            <div class="clearfix"></div>
            <footer>
                <div class="container-fluid">
                    <p class="copyright">&copy; <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados - Pizza Bohemia.</p>
                </div>
            </footer>
        </div>
        <!-- END WRAPPER -->
        <!-- Javascript -->
        <script src="../assets/vendor/jquery/jquery.min.js"></script>
        <script src="../assets/scripts/scripts.js"></script>
        <script src="../assets/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="../assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
        <script src="../assets/vendor/chartist/js/chartist.min.js"></script>
        <script src="../assets/scripts/klorofil-common.js"></script>        
    </body>
</html>