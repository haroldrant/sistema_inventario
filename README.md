# Titulo del proyecto
#### Sistema de inventario

## Índice
1. [Características](#caracteristicas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalacion)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución](#institución)

#### Características
- Uso de Css recomendado: [Ver](https://gitlab.com/haroldrant/curriculum-vitae/-/tree/master/css)
#### Contenido del proyecto
- [index.html](https://gitlab.com/haroldrant/curriculum-vitae/-/blob/master/index.html) : Este archivo muestra la página principal.
#### Tecnologías
- Uso de html [Puede aprender html aquí](https://www.youtube.com/watch?v=kN1XP-Bef7w&t=1s&ab_channel=SoyDalto)
- Uso de CSS [Puede aprender css aquí](https://www.youtube.com/watch?v=OWKXEJN67FE&ab_channel=SoyDalto)
- Uso de JSP [Puede aprender JSP aquí](https://www.youtube.com/watch?v=ed-OyXDxNjM&ab_channel=pildorasinformaticas)
#### IDE
- [Netbeans](https://netbeans.apache.org/download/nb125/nb125.html)
#### Instalación
1. local
    - Descarga el repositorio ubicado en [Descargar](https://gitlab.com/haroldrant/sistema_inventario)
    - Abrir el archivo index.html desde el navegador [Abrir](index.html)
2. gitlab
    - Realizando un fork
#### Demo
Se puede visualizar la versión demo [aquí](https://haroldrant.gitlab.io/curriculum-vitae)
#### Autor(es)
Realizado por: [Harold Rueda Antolinez](<haroldrant@ufps.edu.co>)
[Oscar Bayona](<oscarivanbdia@ufps.edu.co >)
[Javier Alonso](<javieralonsopcab@ufps.edu.co>)
#### Institución
Proyecto desarrollado en la materia de Seminario Integrador II de [Ingeniería de Sistemas](https://ingsistemas.cloud.ufps.edu.co/) en la [Universidad Francisco de Paula Santander](https://ww2.ufps.edu.co/)
