package Controller;

import Dao.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Validar extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("usuario");
        String contra = request.getParameter("contra");
        UserDao dao = new UserDao();
        if (dao.iniciar(nombre, contra)) {
            HttpSession sesion = request.getSession();
            sesion.setAttribute("user", nombre);
            PrintWriter pw = response.getWriter();
            response.setContentType("text/html");
            pw.println("<script type=\"text/javascript\">");
            pw.println("alert('Inicio de sesión, correcto');");
            pw.println("</script>");
            request.getRequestDispatcher("/Dashboard/index.jsp").include(request, response);
        } else {            
            request.getRequestDispatcher("../html/error.html").include(request, response);
        }
    }
}
