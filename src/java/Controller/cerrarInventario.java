/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.HistorialDao;
import Dao.IngredienteDao;
import Dto.Historialinventario;
import Dto.HistorialinventarioPK;
import Dto.Ingrediente;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class cerrarInventario extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        IngredienteDao dao = new IngredienteDao();
        List<Ingrediente> i = dao.getIngredientes();
        HistorialDao hdao = new HistorialDao();
        LocalDate now = LocalDate.now();
        Date d = Date.valueOf(now);
        Calendar ca = Calendar.getInstance();
        ca.setTime(d);
        try {
            for (Ingrediente in : i) {
                HistorialinventarioPK pk = new HistorialinventarioPK(in.getId(), ca, "finalizado");
                Historialinventario his = new Historialinventario(pk, in.getCantidad());
                hdao.create(his);
            }
        } catch (Exception ex) {
            Logger.getLogger(cerrarInventario.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("MostrarIngrediente.do");
    }

}
