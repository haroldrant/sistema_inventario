/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.CompraDao;
import Dao.ProductoDao;
import Dto.Compras;
import Dto.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class compra extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.valueOf(request.getParameter("id"));
        ProductoDao dao = new ProductoDao();
        CompraDao cdao = new CompraDao();
        Producto p = dao.getProducto(id);
        Compras c = new Compras(0, p.getPrecio());
        c.setIdProducto(p);
        cdao.create(c);
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        pw.println("<script type=\"text/javascript\">");
        pw.println("alert('Compra realizada correctamente');");
        pw.println("</script>");
        request.getRequestDispatcher("registroVentas.jsp").include(request, response);
    }

}
