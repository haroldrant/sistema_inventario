package Controller;

import Dao.HistorialDao;
import Dao.IngredienteDao;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MostrarIngrediente extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        IngredienteDao dao = new IngredienteDao();
        HistorialDao hdao = new HistorialDao();
        LocalDate now = LocalDate.now();
        Date d = Date.valueOf(now.toString());
        request.getSession().setAttribute("ingredientes", dao.getIngredientes());
        try {
            request.getSession().setAttribute("historial", hdao.getHistorialHoy(d, "inicio"));
            request.getSession().setAttribute("historialFin", hdao.getHistorialHoy(d, "finalizado"));
        } catch (SQLException ex) {
            Logger.getLogger(MostrarIngrediente.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("registroInventario.jsp");
    }

}
