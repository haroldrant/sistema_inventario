package Controller;

import Dao.HistorialDao;
import Dao.PreparacionDao;
import Dao.ProductoDao;
import Dto.Producto;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MostrarPro extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductoDao dao = new ProductoDao();
        HistorialDao hdao = new HistorialDao();
        PreparacionDao pdao = new PreparacionDao();

        LocalDate now = LocalDate.now();
        Date d = Date.valueOf(now.toString());
        List<Producto> lista = dao.getProductos();
        List<Boolean> preparar = new ArrayList();

        for (Producto p : lista) {
            try {
                preparar.add(pdao.getPreparacion(p));
            } catch (SQLException ex) {
                Logger.getLogger(MostrarPro.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        request.getSession().setAttribute("producto", lista);
        request.getSession().setAttribute("preparado", preparar);
        try {
            request.getSession().setAttribute("historial", hdao.getHistorialHoy(d, "inicio"));
            request.getSession().setAttribute("historialFin", hdao.getHistorialHoy(d, "finalizado"));
        } catch (SQLException ex) {
            Logger.getLogger(MostrarPro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
