package Controller;

import Dao.HistorialDao;
import Dao.IngredienteDao;
import Dto.Historialinventario;
import Dto.HistorialinventarioPK;
import Dto.Ingrediente;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class updateInventario extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] ids = request.getParameterValues("id");
        //String[] names = request.getParameterValues("nombre");
        String[] cants = request.getParameterValues("cantidad");
        IngredienteDao dao = new IngredienteDao();
        HistorialDao hdao = new HistorialDao();
        LocalDate now = LocalDate.now();  
        Date d = Date.valueOf(now);
        Calendar ca = Calendar.getInstance();
        ca.setTime(d);
        for (int i = 0; i < ids.length; i++) {
            try {
                Ingrediente ing = new Ingrediente(Integer.parseInt(ids[i]));
                ing.setCantidad(Float.parseFloat(cants[i]));
                dao.updateIngredientes(ing);
                HistorialinventarioPK pk = new HistorialinventarioPK(ing.getId(),ca,"inicio");
                Historialinventario his = new Historialinventario(pk, ing.getCantidad());
                hdao.create(his);
            } catch (Exception ex) {
                ex.getStackTrace();
            }
        }
        response.sendRedirect("MostrarIngrediente.do");
    }
}
