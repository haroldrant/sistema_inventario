/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Dto.Ingrediente;
import Dao.IngredienteDao;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class venta extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] ids = request.getParameterValues("id");
        //String[] names = request.getParameterValues("nombre");
        String[] cants = request.getParameterValues("cantidad");
        IngredienteDao dao = new IngredienteDao();
        for(int i=0; i<ids.length; i++){
            try {
                Ingrediente ing = new Ingrediente(Integer.parseInt(ids[i]));
                ing.setCantidad(Float.parseFloat(cants[i]));
                dao.updateIngredientes(ing);
            } catch (Exception ex) {
                Logger.getLogger(venta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         
    }
}
