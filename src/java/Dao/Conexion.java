/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Harold
 */
public class Conexion {

    private static Conexion con;
    private EntityManagerFactory bd;

    private Conexion() {
        this.bd = Persistence.createEntityManagerFactory("Inventario_PU");
    }

    public static Conexion getCon() {
        if (con == null) {
            con = new Conexion();
        }
        return con;
    }

    public EntityManagerFactory getBd() {
        return bd;
    }
}
