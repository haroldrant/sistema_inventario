package Dao;

import Dto.Ingrediente;
import Dto.IngredienteJpaController;
import java.util.List;

public class IngredienteDao {

    private IngredienteJpaController jpa;

    public IngredienteDao() {
        Conexion con = Conexion.getCon();
        this.jpa = new IngredienteJpaController(con.getBd());
    }
    
    public List<Ingrediente> getIngredientes(){
        return this.jpa.findIngredienteEntities();
    }
    
    public void updateIngredientes(Ingrediente ingrediente) throws Exception{
        this.jpa.edit(ingrediente);
    }
    
    public Ingrediente getIngrediente(int id){
        return this.jpa.findIngrediente(id);
    }
            
}
