/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.User;
import Dto.UserJpaController;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Admin
 */
public class UserDao {

    private UserJpaController jpa;

    public UserDao() {
        Conexion con = Conexion.getCon();
        this.jpa = new UserJpaController(con.getBd());
    }

    public User getUser(String id) {
        return this.jpa.findUser(id);
    }

    public boolean iniciar(String username, String contra) {
        contra = this.encriptar(contra);
        User u = this.getUser(username);
        if (u != null) {
            if (u.getContra().equals(contra)) {
                return true;
            }
        }
        return false;

    }

    /**
     * Método para encriptar contraseña
     */
    private String encriptar(String contra) {
        String encriptMD5 = DigestUtils.md5Hex(contra);
        return encriptMD5;
    }
}
