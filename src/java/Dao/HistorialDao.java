package Dao;

import Dto.Historialinventario;
import Dto.HistorialinventarioJpaController;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public class HistorialDao {

    private HistorialinventarioJpaController jpa;

    public HistorialDao() {
        Conexion con = Conexion.getCon();
        this.jpa = new HistorialinventarioJpaController(con.getBd());
    }

    public List<Historialinventario> getHistorial() {
        return this.jpa.findHistorialinventarioEntities();
    }

    public Historialinventario getHistorialHoy(Date hoy, String estado) throws SQLException {
        return this.jpa.findHistorialDiario(hoy, estado);
    }

    public void create(Historialinventario h) throws Exception {
        this.jpa.crear(h);
    }
}
