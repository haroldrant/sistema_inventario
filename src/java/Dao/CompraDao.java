/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Compras;
import Dto.ComprasJpaController;

/**
 *
 * @author Admin
 */
public class CompraDao {

    private ComprasJpaController jpa;

    public CompraDao() {
        Conexion con = Conexion.getCon();
        this.jpa = new ComprasJpaController(con.getBd());
    }
    
    public void create(Compras c){
        this.jpa.create(c);
    }
}