/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Producto;
import Dto.ProductoJpaController;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ProductoDao {

    private ProductoJpaController jpa;

    public ProductoDao() {
        Conexion con = Conexion.getCon();
        this.jpa = new ProductoJpaController(con.getBd());
    }
    
    public List<Producto> getProductos(){
        return this.jpa.findProductoEntities();
    }
    
    public Producto getProducto(int id){
        return this.jpa.findProducto(id);
    }
}
