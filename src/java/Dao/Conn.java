/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Admin
 */
public class Conn {

    private static Connection conn;
    private static final String driver = "com.mysql.jdbc.Driver";

    public Conn() {

        conn = null;

        try {

            Class.forName(driver);
            conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/pizza_inventario", "root", "");
            if (conn != null) {
                System.out.println("Conexion establecida");

            }

        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error de conexion " + e);

        }
    }

    public Connection getConnection() {

        return conn;
    }

    public void desconectar() {

        conn = null;

        if (conn == null) {
            System.out.println("Conexion Terminada");
        }

    }
}
