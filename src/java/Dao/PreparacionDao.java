/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Ingrediente;
import Dto.Preparacionproducto;
import Dto.PreparacionproductoJpaController;
import Dto.Producto;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Admin
 */
public class PreparacionDao {

    private PreparacionproductoJpaController jpa;

    public PreparacionDao() {
        Conexion con = Conexion.getCon();
        jpa = new PreparacionproductoJpaController(con.getBd());
    }

    public List<Preparacionproducto> findPreparacion(Producto p) throws SQLException {
        return this.jpa.findPreparacion(p.getId());
    }

    public boolean getPreparacion(Producto p) throws SQLException {
        List<Preparacionproducto> find = this.findPreparacion(p);
        IngredienteDao dao = new IngredienteDao();
        boolean preparado = true;
        for (Preparacionproducto pre : find) {
            Ingrediente preI = dao.getIngrediente(pre.getPreparacionproductoPK().getIdIngrediente());
            if (pre.getCantidad() > preI.getCantidad()) {
                preparado = false;
                break;
            }
        }
        return preparado;
    }

}
