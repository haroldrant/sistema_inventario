/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "historialinventario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Historialinventario.findAll", query = "SELECT h FROM Historialinventario h"),
    @NamedQuery(name = "Historialinventario.findByIdIngrediente", query = "SELECT h FROM Historialinventario h WHERE h.historialinventarioPK.idIngrediente = :idIngrediente"),
    @NamedQuery(name = "Historialinventario.findByFecha", query = "SELECT h FROM Historialinventario h WHERE h.historialinventarioPK.fecha = :fecha"),
    @NamedQuery(name = "Historialinventario.findByEstado", query = "SELECT h FROM Historialinventario h WHERE h.historialinventarioPK.estado = :estado"),
    @NamedQuery(name = "Historialinventario.findByCantidad", query = "SELECT h FROM Historialinventario h WHERE h.cantidad = :cantidad")})
public class Historialinventario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HistorialinventarioPK historialinventarioPK;
    @Basic(optional = false)
    @Column(name = "cantidad")
    private float cantidad;
    @JoinColumn(name = "idIngrediente", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingrediente ingrediente;

    public Historialinventario() {
    }

    public Historialinventario(HistorialinventarioPK historialinventarioPK) {
        this.historialinventarioPK = historialinventarioPK;
    }

    public Historialinventario(HistorialinventarioPK historialinventarioPK, float cantidad) {
        this.historialinventarioPK = historialinventarioPK;
        this.cantidad = cantidad;
    }

    public Historialinventario(int idIngrediente, Calendar fecha, String estado) {
        this.historialinventarioPK = new HistorialinventarioPK(idIngrediente, fecha, estado);
    }

    public HistorialinventarioPK getHistorialinventarioPK() {
        return historialinventarioPK;
    }

    public void setHistorialinventarioPK(HistorialinventarioPK historialinventarioPK) {
        this.historialinventarioPK = historialinventarioPK;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(Ingrediente ingrediente) {
        this.ingrediente = ingrediente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (historialinventarioPK != null ? historialinventarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Historialinventario)) {
            return false;
        }
        Historialinventario other = (Historialinventario) object;
        if ((this.historialinventarioPK == null && other.historialinventarioPK != null) || (this.historialinventarioPK != null && !this.historialinventarioPK.equals(other.historialinventarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Historialinventario[ historialinventarioPK=" + historialinventarioPK + " ]";
    }
    
}
