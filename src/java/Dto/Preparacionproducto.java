/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "preparacionproducto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preparacionproducto.findAll", query = "SELECT p FROM Preparacionproducto p"),
    @NamedQuery(name = "Preparacionproducto.findByIdIngrediente", query = "SELECT p FROM Preparacionproducto p WHERE p.preparacionproductoPK.idIngrediente = :idIngrediente"),
    @NamedQuery(name = "Preparacionproducto.findByIdProducto", query = "SELECT p FROM Preparacionproducto p WHERE p.preparacionproductoPK.idProducto = :idProducto"),
    @NamedQuery(name = "Preparacionproducto.findByCantidad", query = "SELECT p FROM Preparacionproducto p WHERE p.cantidad = :cantidad")})
public class Preparacionproducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreparacionproductoPK preparacionproductoPK;
    @Basic(optional = false)
    @Column(name = "cantidad")
    private int cantidad;
    @JoinColumn(name = "idIngrediente", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingrediente ingrediente;
    @JoinColumn(name = "idProducto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;

    public Preparacionproducto() {
    }

    public Preparacionproducto(PreparacionproductoPK preparacionproductoPK) {
        this.preparacionproductoPK = preparacionproductoPK;
    }

    public Preparacionproducto(PreparacionproductoPK preparacionproductoPK, int cantidad) {
        this.preparacionproductoPK = preparacionproductoPK;
        this.cantidad = cantidad;
    }

    public Preparacionproducto(int idIngrediente, int idProducto) {
        this.preparacionproductoPK = new PreparacionproductoPK(idIngrediente, idProducto);
    }

    public PreparacionproductoPK getPreparacionproductoPK() {
        return preparacionproductoPK;
    }

    public void setPreparacionproductoPK(PreparacionproductoPK preparacionproductoPK) {
        this.preparacionproductoPK = preparacionproductoPK;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(Ingrediente ingrediente) {
        this.ingrediente = ingrediente;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preparacionproductoPK != null ? preparacionproductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preparacionproducto)) {
            return false;
        }
        Preparacionproducto other = (Preparacionproducto) object;
        if ((this.preparacionproductoPK == null && other.preparacionproductoPK != null) || (this.preparacionproductoPK != null && !this.preparacionproductoPK.equals(other.preparacionproductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Preparacionproducto[ preparacionproductoPK=" + preparacionproductoPK + " ]";
    }
    
}
