/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import Dao.Conn;
import Dto.exceptions.NonexistentEntityException;
import Dto.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Admin
 */
public class HistorialinventarioJpaController implements Serializable {

    public HistorialinventarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Historialinventario historialinventario) throws PreexistingEntityException, Exception {
        if (historialinventario.getHistorialinventarioPK() == null) {
            historialinventario.setHistorialinventarioPK(new HistorialinventarioPK());
        }
        historialinventario.getHistorialinventarioPK().setIdIngrediente(historialinventario.getIngrediente().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingrediente ingrediente = historialinventario.getIngrediente();
            if (ingrediente != null) {
                ingrediente = em.getReference(ingrediente.getClass(), ingrediente.getId());
                historialinventario.setIngrediente(ingrediente);
            }
            em.persist(historialinventario);
            if (ingrediente != null) {
                ingrediente.getHistorialinventarioList().add(historialinventario);
                ingrediente = em.merge(ingrediente);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHistorialinventario(historialinventario.getHistorialinventarioPK()) != null) {
                throw new PreexistingEntityException("Historialinventario " + historialinventario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Historialinventario historialinventario) throws NonexistentEntityException, Exception {
        historialinventario.getHistorialinventarioPK().setIdIngrediente(historialinventario.getIngrediente().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Historialinventario persistentHistorialinventario = em.find(Historialinventario.class, historialinventario.getHistorialinventarioPK());
            Ingrediente ingredienteOld = persistentHistorialinventario.getIngrediente();
            Ingrediente ingredienteNew = historialinventario.getIngrediente();
            if (ingredienteNew != null) {
                ingredienteNew = em.getReference(ingredienteNew.getClass(), ingredienteNew.getId());
                historialinventario.setIngrediente(ingredienteNew);
            }
            historialinventario = em.merge(historialinventario);
            if (ingredienteOld != null && !ingredienteOld.equals(ingredienteNew)) {
                ingredienteOld.getHistorialinventarioList().remove(historialinventario);
                ingredienteOld = em.merge(ingredienteOld);
            }
            if (ingredienteNew != null && !ingredienteNew.equals(ingredienteOld)) {
                ingredienteNew.getHistorialinventarioList().add(historialinventario);
                ingredienteNew = em.merge(ingredienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                HistorialinventarioPK id = historialinventario.getHistorialinventarioPK();
                if (findHistorialinventario(id) == null) {
                    throw new NonexistentEntityException("The historialinventario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(HistorialinventarioPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Historialinventario historialinventario;
            try {
                historialinventario = em.getReference(Historialinventario.class, id);
                historialinventario.getHistorialinventarioPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The historialinventario with id " + id + " no longer exists.", enfe);
            }
            Ingrediente ingrediente = historialinventario.getIngrediente();
            if (ingrediente != null) {
                ingrediente.getHistorialinventarioList().remove(historialinventario);
                ingrediente = em.merge(ingrediente);
            }
            em.remove(historialinventario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Historialinventario> findHistorialinventarioEntities() {
        return findHistorialinventarioEntities(true, -1, -1);
    }

    public List<Historialinventario> findHistorialinventarioEntities(int maxResults, int firstResult) {
        return findHistorialinventarioEntities(false, maxResults, firstResult);
    }

    private List<Historialinventario> findHistorialinventarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Historialinventario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Historialinventario findHistorialinventario(HistorialinventarioPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Historialinventario.class, id);
        } finally {
            em.close();
        }
    }

    public int getHistorialinventarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Historialinventario> rt = cq.from(Historialinventario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public void crear(Historialinventario h) throws SQLException {
        Conn cc = new Conn();
        Connection c = cc.getConnection();
        try {
            PreparedStatement statement = c.prepareStatement("INSERT INTO historialinventario(idIngrediente, fecha, estado, cantidad) VALUES(?,?,?,?)");
            statement.setInt(1, h.getHistorialinventarioPK().getIdIngrediente());
            statement.setDate(2, new Date(h.getHistorialinventarioPK().getFecha().getTimeInMillis()));
            statement.setString(3, h.getHistorialinventarioPK().getEstado());
            statement.setFloat(4, h.getCantidad());
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            c.close();
        }
    }

    public Historialinventario findHistorialDiario(Date hoy, String estado) throws SQLException {
        Conn cc = new Conn();
        Connection c = cc.getConnection();
        Historialinventario h = null;
        try {
            PreparedStatement statement = c.prepareStatement("SELECT * FROM historialinventario WHERE fecha=? AND estado=?");
            statement.setDate(1, hoy);
            statement.setString(2, estado);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Calendar ca = Calendar.getInstance();
                ca.setTime(rs.getDate("fecha"));
                HistorialinventarioPK pk = new HistorialinventarioPK(rs.getInt("idIngrediente"), ca, rs.getString("estado"));
                h = new Historialinventario(pk, rs.getFloat("cantidad"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.close();
        }
       
        return h;
    }

}
