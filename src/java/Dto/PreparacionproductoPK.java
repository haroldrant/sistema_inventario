/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Admin
 */
@Embeddable
public class PreparacionproductoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "idIngrediente")
    private int idIngrediente;
    @Basic(optional = false)
    @Column(name = "idProducto")
    private int idProducto;

    public PreparacionproductoPK() {
    }

    public PreparacionproductoPK(int idIngrediente, int idProducto) {
        this.idIngrediente = idIngrediente;
        this.idProducto = idProducto;
    }

    public int getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(int idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idIngrediente;
        hash += (int) idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreparacionproductoPK)) {
            return false;
        }
        PreparacionproductoPK other = (PreparacionproductoPK) object;
        if (this.idIngrediente != other.idIngrediente) {
            return false;
        }
        if (this.idProducto != other.idProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.PreparacionproductoPK[ idIngrediente=" + idIngrediente + ", idProducto=" + idProducto + " ]";
    }
    
}
