/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Admin
 */
@Embeddable
public class HistorialinventarioPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "idIngrediente")
    private int idIngrediente;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Calendar fecha;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;

    public HistorialinventarioPK() {
    }

    public HistorialinventarioPK(int idIngrediente, Calendar fecha, String estado) {
        this.idIngrediente = idIngrediente;
        this.fecha = fecha;
        this.estado = estado;
    }

    public int getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(int idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idIngrediente;
        hash += (fecha != null ? fecha.hashCode() : 0);
        hash += (estado != null ? estado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialinventarioPK)) {
            return false;
        }
        HistorialinventarioPK other = (HistorialinventarioPK) object;
        if (this.idIngrediente != other.idIngrediente) {
            return false;
        }
        if ((this.fecha == null && other.fecha != null) || (this.fecha != null && !this.fecha.equals(other.fecha))) {
            return false;
        }
        if ((this.estado == null && other.estado != null) || (this.estado != null && !this.estado.equals(other.estado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.HistorialinventarioPK[ idIngrediente=" + idIngrediente + ", fecha=" + fecha + ", estado=" + estado + " ]";
    }
    
}
