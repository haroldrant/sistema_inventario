/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import Dao.Conn;
import Dto.exceptions.NonexistentEntityException;
import Dto.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Admin
 */
public class PreparacionproductoJpaController implements Serializable {

    public PreparacionproductoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Preparacionproducto preparacionproducto) throws PreexistingEntityException, Exception {
        if (preparacionproducto.getPreparacionproductoPK() == null) {
            preparacionproducto.setPreparacionproductoPK(new PreparacionproductoPK());
        }
        preparacionproducto.getPreparacionproductoPK().setIdIngrediente(preparacionproducto.getIngrediente().getId());
        preparacionproducto.getPreparacionproductoPK().setIdProducto(preparacionproducto.getProducto().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingrediente ingrediente = preparacionproducto.getIngrediente();
            if (ingrediente != null) {
                ingrediente = em.getReference(ingrediente.getClass(), ingrediente.getId());
                preparacionproducto.setIngrediente(ingrediente);
            }
            em.persist(preparacionproducto);
            if (ingrediente != null) {
                ingrediente.getPreparacionproductoList().add(preparacionproducto);
                ingrediente = em.merge(ingrediente);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPreparacionproducto(preparacionproducto.getPreparacionproductoPK()) != null) {
                throw new PreexistingEntityException("Preparacionproducto " + preparacionproducto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Preparacionproducto preparacionproducto) throws NonexistentEntityException, Exception {
        preparacionproducto.getPreparacionproductoPK().setIdIngrediente(preparacionproducto.getIngrediente().getId());
        preparacionproducto.getPreparacionproductoPK().setIdProducto(preparacionproducto.getProducto().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Preparacionproducto persistentPreparacionproducto = em.find(Preparacionproducto.class, preparacionproducto.getPreparacionproductoPK());
            Ingrediente ingredienteOld = persistentPreparacionproducto.getIngrediente();
            Ingrediente ingredienteNew = preparacionproducto.getIngrediente();
            if (ingredienteNew != null) {
                ingredienteNew = em.getReference(ingredienteNew.getClass(), ingredienteNew.getId());
                preparacionproducto.setIngrediente(ingredienteNew);
            }
            preparacionproducto = em.merge(preparacionproducto);
            if (ingredienteOld != null && !ingredienteOld.equals(ingredienteNew)) {
                ingredienteOld.getPreparacionproductoList().remove(preparacionproducto);
                ingredienteOld = em.merge(ingredienteOld);
            }
            if (ingredienteNew != null && !ingredienteNew.equals(ingredienteOld)) {
                ingredienteNew.getPreparacionproductoList().add(preparacionproducto);
                ingredienteNew = em.merge(ingredienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PreparacionproductoPK id = preparacionproducto.getPreparacionproductoPK();
                if (findPreparacionproducto(id) == null) {
                    throw new NonexistentEntityException("The preparacionproducto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PreparacionproductoPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Preparacionproducto preparacionproducto;
            try {
                preparacionproducto = em.getReference(Preparacionproducto.class, id);
                preparacionproducto.getPreparacionproductoPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The preparacionproducto with id " + id + " no longer exists.", enfe);
            }
            Ingrediente ingrediente = preparacionproducto.getIngrediente();
            if (ingrediente != null) {
                ingrediente.getPreparacionproductoList().remove(preparacionproducto);
                ingrediente = em.merge(ingrediente);
            }
            em.remove(preparacionproducto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Preparacionproducto> findPreparacionproductoEntities() {
        return findPreparacionproductoEntities(true, -1, -1);
    }

    public List<Preparacionproducto> findPreparacionproductoEntities(int maxResults, int firstResult) {
        return findPreparacionproductoEntities(false, maxResults, firstResult);
    }

    private List<Preparacionproducto> findPreparacionproductoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Preparacionproducto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Preparacionproducto findPreparacionproducto(PreparacionproductoPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Preparacionproducto.class, id);
        } finally {
            em.close();
        }
    }

    public List<Preparacionproducto> findPreparacion(int id) throws SQLException {
        Conn cc = new Conn();
        Connection c = cc.getConnection();
        List<Preparacionproducto> preparacion = new ArrayList();
        try {
            PreparedStatement statement = c.prepareStatement("SELECT * FROM preparacionproducto WHERE idproducto=?");            
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                PreparacionproductoPK pk = new PreparacionproductoPK(rs.getInt("idIngrediente"), id);
                Preparacionproducto p = new Preparacionproducto(pk, rs.getInt("cantidad"));
                preparacion.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.close();
        }
        return preparacion;
    }

    public int getPreparacionproductoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Preparacionproducto> rt = cq.from(Preparacionproducto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
