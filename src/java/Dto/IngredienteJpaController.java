/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import Dto.exceptions.IllegalOrphanException;
import Dto.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Admin
 */
public class IngredienteJpaController implements Serializable {

    public IngredienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ingrediente ingrediente) {
        if (ingrediente.getHistorialinventarioList() == null) {
            ingrediente.setHistorialinventarioList(new ArrayList<Historialinventario>());
        }
        if (ingrediente.getPreparacionproductoList() == null) {
            ingrediente.setPreparacionproductoList(new ArrayList<Preparacionproducto>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Historialinventario> attachedHistorialinventarioList = new ArrayList<Historialinventario>();
            for (Historialinventario historialinventarioListHistorialinventarioToAttach : ingrediente.getHistorialinventarioList()) {
                historialinventarioListHistorialinventarioToAttach = em.getReference(historialinventarioListHistorialinventarioToAttach.getClass(), historialinventarioListHistorialinventarioToAttach.getHistorialinventarioPK());
                attachedHistorialinventarioList.add(historialinventarioListHistorialinventarioToAttach);
            }
            ingrediente.setHistorialinventarioList(attachedHistorialinventarioList);
            List<Preparacionproducto> attachedPreparacionproductoList = new ArrayList<Preparacionproducto>();
            for (Preparacionproducto preparacionproductoListPreparacionproductoToAttach : ingrediente.getPreparacionproductoList()) {
                preparacionproductoListPreparacionproductoToAttach = em.getReference(preparacionproductoListPreparacionproductoToAttach.getClass(), preparacionproductoListPreparacionproductoToAttach.getPreparacionproductoPK());
                attachedPreparacionproductoList.add(preparacionproductoListPreparacionproductoToAttach);
            }
            ingrediente.setPreparacionproductoList(attachedPreparacionproductoList);
            em.persist(ingrediente);
            for (Historialinventario historialinventarioListHistorialinventario : ingrediente.getHistorialinventarioList()) {
                Ingrediente oldIngredienteOfHistorialinventarioListHistorialinventario = historialinventarioListHistorialinventario.getIngrediente();
                historialinventarioListHistorialinventario.setIngrediente(ingrediente);
                historialinventarioListHistorialinventario = em.merge(historialinventarioListHistorialinventario);
                if (oldIngredienteOfHistorialinventarioListHistorialinventario != null) {
                    oldIngredienteOfHistorialinventarioListHistorialinventario.getHistorialinventarioList().remove(historialinventarioListHistorialinventario);
                    oldIngredienteOfHistorialinventarioListHistorialinventario = em.merge(oldIngredienteOfHistorialinventarioListHistorialinventario);
                }
            }
            for (Preparacionproducto preparacionproductoListPreparacionproducto : ingrediente.getPreparacionproductoList()) {
                Ingrediente oldIngredienteOfPreparacionproductoListPreparacionproducto = preparacionproductoListPreparacionproducto.getIngrediente();
                preparacionproductoListPreparacionproducto.setIngrediente(ingrediente);
                preparacionproductoListPreparacionproducto = em.merge(preparacionproductoListPreparacionproducto);
                if (oldIngredienteOfPreparacionproductoListPreparacionproducto != null) {
                    oldIngredienteOfPreparacionproductoListPreparacionproducto.getPreparacionproductoList().remove(preparacionproductoListPreparacionproducto);
                    oldIngredienteOfPreparacionproductoListPreparacionproducto = em.merge(oldIngredienteOfPreparacionproductoListPreparacionproducto);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ingrediente ingrediente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
//            String sql = "UPDATE ingrediente SET cantidad = ? WHERE id = ?";
//            Query q = em.createNativeQuery(sql);
//            q.setParameter(1, ingrediente.getCantidad());
//            q.setParameter(2, ingrediente.getId());
//            em.merge(ingrediente);
            Ingrediente ing = em.find(Ingrediente.class, ingrediente.getId());
            ing.setCantidad(ingrediente.getCantidad());
            em.merge(ing);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ingrediente.getId();
                if (findIngrediente(id) == null) {
                    throw new NonexistentEntityException("The ingrediente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            em.close();
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingrediente ingrediente;
            try {
                ingrediente = em.getReference(Ingrediente.class, id);
                ingrediente.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingrediente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Historialinventario> historialinventarioListOrphanCheck = ingrediente.getHistorialinventarioList();
            for (Historialinventario historialinventarioListOrphanCheckHistorialinventario : historialinventarioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ingrediente (" + ingrediente + ") cannot be destroyed since the Historialinventario " + historialinventarioListOrphanCheckHistorialinventario + " in its historialinventarioList field has a non-nullable ingrediente field.");
            }
            List<Preparacionproducto> preparacionproductoListOrphanCheck = ingrediente.getPreparacionproductoList();
            for (Preparacionproducto preparacionproductoListOrphanCheckPreparacionproducto : preparacionproductoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ingrediente (" + ingrediente + ") cannot be destroyed since the Preparacionproducto " + preparacionproductoListOrphanCheckPreparacionproducto + " in its preparacionproductoList field has a non-nullable ingrediente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(ingrediente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ingrediente> findIngredienteEntities() {
        return findIngredienteEntities(true, -1, -1);
    }

    public List<Ingrediente> findIngredienteEntities(int maxResults, int firstResult) {
        return findIngredienteEntities(false, maxResults, firstResult);
    }

    private List<Ingrediente> findIngredienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ingrediente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ingrediente findIngrediente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ingrediente.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngredienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ingrediente> rt = cq.from(Ingrediente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
